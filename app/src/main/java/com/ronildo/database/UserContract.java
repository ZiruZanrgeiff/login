package com.ronildo.database;

import android.provider.BaseColumns;

public final class UserContract implements BaseColumns {
    private UserContract() {}

    public static String TABLE_NAME = "user";

    public static String COLUMN_ID = "id";
    public static String COLUMN_NAME = "name";
    public static String COLUMN_EMAIL = "email";
    public static String COLUMN_PASSWORD = "password";

    public static String SQL_CREAT_TABLE = "CREATE TABLE " + TABLE_NAME + "("
            + COLUMN_ID + "INTEGER PRIMARY KEY,"
            + COLUMN_NAME + " varchar(60), "
            + COLUMN_EMAIL + " varchar(30), "
            + COLUMN_PASSWORD + " varchar(60))";

    public static String SQL_DROP_TABLE = "DROP TABLE " + TABLE_NAME;

}
