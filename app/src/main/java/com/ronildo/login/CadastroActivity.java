package com.ronildo.login;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.ronildo.database.DatabaseHelper;
import com.ronildo.database.UserContract;

public class CadastroActivity extends AppCompatActivity {

    private EditText name;
    private EditText email;
    private EditText password;
    private EditText repeatPassword;

    private DatabaseHelper databaseHelper;

    public void salvar(View view) {
        if(isValid()) {

            SQLiteDatabase db = databaseHelper.getReadableDatabase();
            ContentValues values = new ContentValues();

            values.put(UserContract.COLUMN_NAME, name.getText().toString());
            values.put(UserContract.COLUMN_EMAIL, email.getText().toString());
            values.put(UserContract.COLUMN_PASSWORD, password.getText().toString());

            long rowId = db.insert(UserContract.TABLE_NAME, null, values);

            if(rowId > 0) {
                Toast.makeText(getApplicationContext(), "Salvo com sucesso!", Toast.LENGTH_SHORT).show();
            }
        }

        if(!isValid()) {
            Toast.makeText(getApplicationContext(), "Falha na validação", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isValid() {
        boolean valid = false;

        valid = password.getText().toString().equals(repeatPassword.getText().toString())
                && hasText(password)
                && hasText(name)
                && hasText(email);

        return valid;
    }

    private boolean hasText(EditText editText) {
        return editText.getText().toString().length() > 0;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        databaseHelper = new DatabaseHelper(getApplicationContext());

        name = findViewById(R.id.editTextName);
        email = findViewById(R.id.editTextEmail);
        password = findViewById(R.id.editTextPassword);
        repeatPassword = findViewById(R.id.editTextRepeatPassword);
    }
}
